# frozen_string_literal: true

# Class in charge of represents the Exception for command's invalid arguments
class InvalidArguments < StandardError
  def initialize(msg = 'Invalid Arguments')
    super
  end
end
