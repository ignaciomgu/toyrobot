# frozen_string_literal: true

# Class in charge of represents the Exception for when the given position are out of the board
class OutOfRange < StandardError
  def initialize(msg = 'The positions are out of range')
    super
  end
end
