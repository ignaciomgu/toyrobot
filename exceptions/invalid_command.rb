# frozen_string_literal: true

# Class in charge of represents the Exception for invalid commands
class InvalidCommand < StandardError
  def initialize(msg = 'Invalid Command')
    super
  end
end
