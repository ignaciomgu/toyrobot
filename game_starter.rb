# frozen_string_literal: true

require_relative 'lib/game_orchestrator'
require_relative 'lib/robot'
require_relative 'lib/commands/switch'

# It will instantiate the game orchestrator providing the robot and switch instances
GameOrchestrator.new(Robot.new, Switch.new, Board.new).start
