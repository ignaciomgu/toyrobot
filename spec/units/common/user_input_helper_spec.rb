# frozen_string_literal: true

require_relative '../../../common/user_input_helper'

RSpec.describe 'Command Helper' do
  describe '#parse' do
    context 'when single word valid input' do
      it { expect(UserInputHelper.parse('MOVE')).to eq ['MOVE', {}] }
    end

    context 'when compound word valid input' do
      it { expect(UserInputHelper.parse('PLACE 0,0,NORTH')).to eq ['PLACE', { x: 0, y: 0, direction: 'NORTH' }] }
    end

    context 'when single non-valid input' do
      it { expect(UserInputHelper.parse('asdasd')).to eq ['asdasd', {}] }
    end

    context 'when compound non-valid input' do
      it { expect(UserInputHelper.parse('asdasd asdasd')).to eq ['asdasd', { x: nil, y: nil, direction: nil }] }

      it { expect(UserInputHelper.parse('asdasd x,y,n')).to eq ['asdasd', { x: nil, y: nil, direction: 'n' }] }

      it { expect(UserInputHelper.parse('asdasd 0,0,n')).to eq ['asdasd', { x: 0, y: 0, direction: 'n' }] }

      it { expect(UserInputHelper.parse('asdasd 1,1,NORTH')).to eq ['asdasd', { x: 1, y: 1, direction: 'NORTH' }] }

      it { expect(UserInputHelper.parse('asdasd,1,1,NORTH')).to eq ['asdasd,1,1,NORTH', {}] }

      it { expect(UserInputHelper.parse('PLACE ,1,1,NORTH,1,2')).to eq ['PLACE', { x: nil, y: 1, direction: '1' }] }
    end

    context 'when blank word input' do
      it { expect(UserInputHelper.parse(' ')).to eq ['', {}] }
    end
  end
end
