# frozen_string_literal: true

require_relative '../../../../lib/commands/place'
require_relative '../../../../lib/Robot'

RSpec.describe 'Place' do
  let(:axis_x) { nil }
  let(:axis_y) { nil }
  let(:direction) { nil }
  let(:arguments) do
    {
      x: axis_x,
      y: axis_y,
      direction: direction
    }
  end
  let(:inside) { true }
  let(:board) do
    instance_double('Board', inside?: inside)
  end
  let(:robot) do
    Robot.new
  end
  let(:place_command) { Place.new(board, robot) }

  before do
    place_command.arguments = arguments
  end

  describe '#execute' do
    context 'when coordinates and direction are valid' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }

      it 'place robot within the board' do
        expect(robot).to receive(:place).with(0, 0, 'NORTH')

        place_command.execute
      end
    end

    context 'when coordinates are nil' do
      let(:direction) { 'NORTH' }

      it 'raise an Invalid Arguments error' do
        expect { place_command.execute }.to raise_error('Invalid Arguments')
      end
    end

    context 'when direction is nil' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }

      it 'raise an Invalid Arguments error' do
        expect { place_command.execute }.to raise_error('Invalid Arguments')
      end
    end

    context 'when coordinates are out of range' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }
      let(:inside) { false }

      it 'raise an Out of range error' do
        expect { place_command.execute }.to raise_error('The positions are out of range')
      end
    end
  end

  describe '#to_string' do
    let(:axis_x) { 0 }
    let(:axis_y) { 0 }
    let(:direction) { 'NORTH' }

    it { expect(place_command.to_string).to eq 'Command: "Place" {:x=>0, :y=>0, :direction=>"NORTH"}' }
  end
end
