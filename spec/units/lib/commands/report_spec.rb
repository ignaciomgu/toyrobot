# frozen_string_literal: true

require_relative '../../../../lib/commands/report'
require_relative '../../../../lib/robot'

RSpec.describe 'Report' do
  let(:axis_x) { nil }
  let(:axis_y) { nil }
  let(:direction) { nil }
  let(:robot) do
    Robot.new(axis_x, axis_y, direction)
  end
  let(:report_command) { Report.new(robot) }

  describe '#execute' do
    context 'when robot does not have coordinates nor direction' do
      it 'does not report anything' do
        expect_any_instance_of(Kernel).to receive(:puts).with('Not ready yet')

        report_command.execute
      end
    end

    context 'when robot has coordinates and direction' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }

      it 'reports' do
        expect_any_instance_of(Kernel).to receive(:puts).with('0,0,NORTH')

        report_command.execute
      end
    end
  end

  describe '#to_string' do
    it { expect(report_command.to_string).to eq 'Command: "Report" {}' }
  end
end
