# frozen_string_literal: true

require_relative '../../../../lib/commands/history'

RSpec.describe 'History' do
  let(:history) { [] }
  let(:switch) { instance_double('Switch', history: history) }

  describe '#execute' do
    context 'when there is not history of commands' do
      it 'does not print anything' do
        expect_any_instance_of(Kernel).to receive(:puts).with('No history yet')

        History.new(switch).execute
      end
    end

    context 'when there is history of commands' do
      let(:history) { [instance_double('Move', to_string: 'Move'), instance_double('Left', to_string: 'left')] }
      it 'prints history' do
        expect_any_instance_of(Kernel).to receive(:puts).with(%w[Move left])

        History.new(switch).execute
      end
    end
  end

  describe '#to_string' do
    it { expect(History.new(switch).to_string).to eq 'Command: "History" {}' }
  end
end
