# frozen_string_literal: true

require_relative '../../../../lib/commands/move'
require_relative '../../../../lib/Robot'

RSpec.describe 'Move' do
  let(:axis_x) { 0 }
  let(:axis_y) { 0 }
  let(:direction) { 'NORTH' }
  let(:inside) { true }
  let(:board) do
    instance_double('Board', inside?: inside)
  end
  let(:robot) do
    Robot.new(axis_x, axis_y, direction)
  end
  let(:move_command) { Move.new(board, robot) }

  describe '#execute' do
    context 'when robot has valid coordinates and direction' do
      it 'move robot within the board' do
        expect(robot).to receive(:move)

        move_command.execute
      end
    end

    context 'when coordinates are out of range' do
      let(:inside) { false }

      it 'raise an Out of Range error' do
        expect { move_command.execute }.to raise_error('The positions are out of range')
      end
    end
  end

  describe '#to_string' do
    it { expect(move_command.to_string).to eq 'Command: "Move" {}' }
  end
end
