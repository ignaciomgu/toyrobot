# frozen_string_literal: true

require_relative '../../../../lib/commands/switch'
require_relative '../../../../lib/commands/place'
require_relative '../../../../lib/commands/move'
require_relative '../../../../lib/commands/left'
require_relative '../../../../lib/commands/right'
require_relative '../../../../exceptions/invalid_command'

RSpec.describe 'Switch' do
  let(:switch) { Switch.new }

  describe '#execute' do
    context 'when nil command param' do
      it 'raises an Invalid Command error' do
        expect { switch.execute(nil) }.to raise_error(InvalidCommand)

        expect(switch.history).to eq []
      end
    end

    context 'when execute command with arguments' do
      it 'execute and store the command successfully' do
        place_command = Place.new(double, double)

        expect(place_command).to receive(:execute)

        switch.execute(place_command, { x: 0, y: 0, direction: 'NORTH' })

        expect(switch.history).to eq [place_command]
      end
    end

    context 'when execute single command' do
      it 'execute and store the command successfully' do
        move_command = Move.new(double, double)

        expect(move_command).to receive(:execute)

        switch.execute(move_command)

        expect(switch.history).to eq [move_command]
      end
    end

    context 'when the command history exceeds the limit' do
      it 'delete last command' do
        stub_const('Switch::HISTORY_LIMIT', 2)

        move_command = Move.new(double, double)
        left_command = Left.new(double)
        right_command = Right.new(double)

        expect(move_command).to receive(:execute)
        expect(left_command).to receive(:execute)
        expect(right_command).to receive(:execute)

        switch.execute(move_command)
        switch.execute(left_command)
        switch.execute(right_command)

        expect(switch.history).to eq [left_command, right_command]
      end
    end
  end
end
