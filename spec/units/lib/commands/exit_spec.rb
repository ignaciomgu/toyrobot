# frozen_string_literal: true

require_relative '../../../../lib/commands/exit'

RSpec.describe 'Exit' do
  describe '#execute' do
    it 'terminates execution successfully' do
      expect_any_instance_of(Kernel).to receive(:puts).with('Bye!')

      expect { Exit.new.execute }.to raise_error(SystemExit) do |error|
        expect(error.status).to eq(0)
      end
    end
  end

  describe '#to_string' do
    it { expect(Exit.new.to_string).to eq 'Command: "Exit" {}' }
  end
end
