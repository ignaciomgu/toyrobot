# frozen_string_literal: true

require_relative '../../../../lib/commands/left'
require_relative '../../../../lib/Robot'

RSpec.describe 'Left' do
  let(:axis_x) { 0 }
  let(:axis_y) { 0 }
  let(:direction) { 'NORTH' }
  let(:robot) do
    Robot.new(axis_x, axis_y, direction)
  end
  let(:turn_left_command) { Left.new(robot) }

  describe '#execute' do
    context 'when robot has valid coordinates and direction' do
      it 'turns robot to the left' do
        turn_left_command.execute

        expect(robot.direction).to eq 'WEST'
      end
    end

    context 'when robot does not have coordinates' do
      let(:axis_x) { nil }
      let(:axis_y) { nil }

      it 'does not turn to left' do
        turn_left_command.execute

        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot does not have direction' do
      let(:direction) { nil }

      it 'does not turn to left' do
        turn_left_command.execute

        expect(robot.direction).to be_nil
      end
    end
  end

  describe '#to_string' do
    it { expect(turn_left_command.to_string).to eq 'Command: "Left" {}' }
  end
end
