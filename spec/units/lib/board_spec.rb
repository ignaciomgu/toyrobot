# frozen_string_literal: true

require_relative '../../../lib/board'

RSpec.describe 'Board' do
  let(:rows) { 3 }
  let(:columns) { 3 }
  let(:board) { Board.new(rows, columns) }

  describe '#inside?' do
    context 'when x overtakes the limit' do
      it { expect(board.inside?(3, 0)).to be_falsey }
    end

    context 'when x undertakes the limit' do
      it { expect(board.inside?(-1, 0)).to be_falsey }
    end

    context 'when y overtakes the limit' do
      it { expect(board.inside?(0, 3)).to be_falsey }
    end

    context 'when y undertakes the limit' do
      it { expect(board.inside?(0, -1)).to be_falsey }
    end

    context 'when both coordinates overtake the limit' do
      it { expect(board.inside?(3, 3)).to be_falsey }
    end

    context 'when both coordinates undertake the limit' do
      it { expect(board.inside?(-1, -1)).to be_falsey }
    end

    context 'when both coordinates are inside' do
      it { expect(board.inside?(0, 0)).to be_truthy }
    end

    context 'when both coordinates are inside' do
      it { expect(board.inside?(nil, nil)).to be_falsey }
    end

    context 'when x is nil' do
      it { expect(board.inside?(nil, 3)).to be_falsey }
    end

    context 'when y is nil' do
      it { expect(board.inside?(0, nil)).to be_falsey }
    end
  end
end
