# frozen_string_literal: true

require_relative '../../../lib/robot'

RSpec.describe 'Robot' do
  let(:axis_x) { nil }
  let(:axis_y) { nil }
  let(:direction) { nil }
  let(:robot) { Robot.new(axis_x, axis_y, direction) }

  describe '#place' do
    before do
      robot.place(1, 2, 'NORTH')
    end

    shared_examples 'placement successful' do
      it 'place the robot in the given coordinates and direction' do
        expect(robot.axis_x).to eq 1
        expect(robot.axis_y).to eq 2
        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'placement successful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'SOUTH' }

      it_behaves_like 'placement successful'
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }

      it_behaves_like 'placement successful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }
      let(:direction) { 'SOUTH' }

      it_behaves_like 'placement successful'
    end
  end

  describe '#move' do
    before do
      robot.move
    end

    context 'when robot does not contain coordinates nor direction' do
      it 'does not move anywhere' do
        expect(robot.axis_x).to be_nil
        expect(robot.axis_y).to be_nil
        expect(robot.direction).to be_nil
      end
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'NORTH' }

      it 'does not move anywhere' do
        expect(robot.axis_x).to be_nil
        expect(robot.axis_y).to be_nil
        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }

      it 'does not move anywhere' do
        expect(robot.axis_x).to eq 0
        expect(robot.axis_y).to eq 0
        expect(robot.direction).to be_nil
      end
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 0 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }

      it 'moves successfully' do
        expect(robot.axis_x).to eq 0
        expect(robot.axis_y).to eq 1
        expect(robot.direction).to eq 'NORTH'
      end
    end
  end

  describe '#next_x' do
    shared_examples 'next x unsuccessful' do
      it 'does not have next x' do
        expect(robot.next_x).to be_nil
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'next x unsuccessful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'WEST' }

      it_behaves_like 'next x unsuccessful'
    end

    context 'when robot does not containdirection' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }

      it_behaves_like 'next x unsuccessful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }
      let(:direction) { 'WEST' }

      it 'it has next x' do
        expect(robot.next_x).to eq 1
      end
    end
  end

  describe '#next_y' do
    shared_examples 'next y unsuccessful' do
      it 'does not have next y' do
        expect(robot.next_y).to be_nil
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'next y unsuccessful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'NORTH' }

      it_behaves_like 'next y unsuccessful'
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }

      it_behaves_like 'next y unsuccessful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }

      it 'it has next y' do
        expect(robot.next_y).to eq 1
      end
    end
  end

  describe '#turn_right' do
    before do
      robot.turn_right
    end

    shared_examples 'turn right unsuccessful' do
      it 'does not turn right' do
        expect(robot.direction).to be_nil
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'turn right unsuccessful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'NORTH' }

      it 'does not turn right' do
        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }

      it_behaves_like 'turn right unsuccessful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }
      let(:direction) { 'NORTH' }

      it 'turns right' do
        expect(robot.direction).to eq 'EAST'
      end
    end
  end

  describe '#turn_left' do
    before do
      robot.turn_left
    end

    shared_examples 'turn left unsuccessful' do
      it 'does not turn left' do
        expect(robot.direction).to be_nil
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'turn left unsuccessful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'NORTH' }

      it 'does not turn left' do
        expect(robot.direction).to eq 'NORTH'
      end
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }

      it_behaves_like 'turn left unsuccessful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }
      let(:direction) { 'EAST' }

      it 'turns left' do
        expect(robot.direction).to eq 'NORTH'
      end
    end
  end

  describe '#report' do
    shared_examples 'report unsuccessful' do
      it 'does report' do
        expect(robot.report).to be_nil
      end
    end

    context 'when robot does not contain coordinates nor direction' do
      it_behaves_like 'report unsuccessful'
    end

    context 'when robot does not contain coordinates' do
      let(:direction) { 'NORTH' }

      it_behaves_like 'report unsuccessful'
    end

    context 'when robot does not contain direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }

      it_behaves_like 'report unsuccessful'
    end

    context 'when robot contains coordinates and direction' do
      let(:axis_x) { 2 }
      let(:axis_y) { 0 }
      let(:direction) { 'EAST' }

      it 'reports successfully' do
        expect(robot.report).to eq '2,0,EAST'
      end
    end
  end
end
