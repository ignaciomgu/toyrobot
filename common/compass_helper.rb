# frozen_string_literal: true

class CompassHelper
  # Different valid directions
  DIRECTIONS = %w[NORTH SOUTH EAST WEST].freeze

  TO_RIGHT = {
    'NORTH' => 'EAST',
    'EAST' => 'SOUTH',
    'SOUTH' => 'WEST',
    'WEST' => 'NORTH'
  }.freeze

  TO_LEFT = {
    'NORTH' => 'WEST',
    'EAST' => 'NORTH',
    'SOUTH' => 'EAST',
    'WEST' => 'SOUTH'
  }.freeze

  TO_X = {
    'NORTH' => 0,
    'EAST' => 1,
    'SOUTH' => 0,
    'WEST' => -1
  }.freeze

  TO_Y = {
    'NORTH' => 1,
    'EAST' => 0,
    'SOUTH' => -1,
    'WEST' => 0
  }.freeze
end
