# frozen_string_literal: true

# This class is in charge of parsing every user input.
class UserInputHelper
  # Given a raw string, it parses based on the expected command_inputs way.
  #
  # @param [String] user input
  # @return [Array<String, Hash>] the contents reversed lexically
  def self.parse(command_input)
    command_input_pieces = command_input.split(' ') || []
    command_input_name = command_input_pieces.size >= 1 ? command_input_pieces[0] : ''

    # If there is no arguments, it returns the single command_input
    return [command_input_name, {}] if command_input_pieces.size < 2 || command_input_pieces[1].size < 3

    # Picks arguments
    command_input_arguments = command_input_pieces[1].split(',')

    x = Integer(command_input_arguments[0]) rescue nil
    y = Integer(command_input_arguments[1]) rescue nil
    direction = command_input_arguments[2]

    [command_input_name, { x: x, y: y, direction: direction }]
  end
end
