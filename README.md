# Toy Robot

More information about the requirements of [the toy robot](https://github.com/bettercaring/toy-robot. 

Quick start guide:

```sh
# Install Ruby with a version manager, rbenv is recommended
# (SEE BELOW: § Dependencies → rbenv)
CFLAGS="-Wno-error=implicit-function-declaration" rbenv install

# Install bundler
gem install bundler -v 2.3.17

# Clone and cd
git clone https://gitlab.com/ignaciomgu/toyrobot.git
cd toyrobot

# Install gems 
bundle install

# Run specs
bundle exec rspec

# Run toyrobot

ruby game_starter.rb
```

Quick Demo:

![Alt Text](demo_toy_robot.gif)