# Represents a board of Row X Column
class Board
  def initialize(rows = 5, columns = 5)
    @rows = rows
    @columns = columns
  end

  # Checks if the given x and y coordinates are placed within the board
  def inside?(axis_x, axis_y)
    return false unless axis_x && axis_y

    axis_x < @rows && axis_x >= 0 && axis_y < @columns && axis_y >= 0
  end
end
