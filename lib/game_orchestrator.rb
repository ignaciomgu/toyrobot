# frozen_string_literal: true

require_relative '../common/user_input_helper'
require_relative '../exceptions/invalid_command'
require_relative 'commands/left'
require_relative 'commands/move'
require_relative 'commands/place'
require_relative 'commands/report'
require_relative 'commands/right'
require_relative 'commands/switch'
require_relative 'commands/history'
require_relative 'commands/exit'
require_relative 'board'

# This class is an orchestrator in charge of managing the whole game
class GameOrchestrator
  def initialize(robot, switch, board)
    @switch = switch
    @board = board
    @robot = robot
    @commands = init_commands
  end

  def start
    display_welcome

    while (input_command = gets.chomp.upcase)
      if input_command.size > 20
        puts 'Input too long'
        next
      end

      perform(input_command)
    end
  end

  private

  def perform(input_command)
    command_name, command_arguments = UserInputHelper.parse(input_command)

    @switch.execute(@commands[command_name], command_arguments)
  rescue StandardError => e
    puts e
  end

  def display_welcome
    puts 'Hey!... before starting, you need to place your robot in the 5x5 game table.' \
         "\nEnter a command:\n'PLACE X,Y,NORTH|SOUTH|EAST|WEST', MOVE, LEFT, RIGHT, REPORT, HISTORY or EXIT"
  end

  def init_commands
    {
      'PLACE' => Place.new(@board, @robot),
      'MOVE' => Move.new(@board, @robot),
      'LEFT' => Left.new(@robot),
      'RIGHT' => Right.new(@robot),
      'REPORT' => Report.new(@robot),
      'HISTORY' => History.new(@switch),
      'EXIT' => Exit.new
    }
  end
end
