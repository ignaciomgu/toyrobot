# Context class for the command pattern, it is a generic class for calling,
# set arguments and executing the commands.
# It also keeps a history of each executed command up to a limit
class Switch
  HISTORY_LIMIT = 30
  attr_reader :history

  def initialize
    @history = []
  end

  def execute(command, arguments = {})
    raise InvalidCommand unless command

    @history << command
    @history.delete_at(0) if @history.size > HISTORY_LIMIT

    command.arguments = arguments if arguments

     command.execute
  end
end
