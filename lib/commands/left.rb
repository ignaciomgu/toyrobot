# frozen_string_literal: true

require_relative 'command'

# Command class in charge of execute the turn to the left of the robot
class Left < Command
  def initialize(robot)
    super()
    @robot = robot
  end

  def execute
    @robot.turn_left
  end
end
