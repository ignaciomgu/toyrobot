# frozen_string_literal: true

require_relative 'command'

# Command class in charge of execute the printing of the command's history
class History < Command
  def initialize(switch)
    super()
    @switch = switch
  end

  def execute
    to_print = @switch.history.map(&:to_string)

    to_print.any? ? puts(to_print) : puts('No history yet')
  end
end

