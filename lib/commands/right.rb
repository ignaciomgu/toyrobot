# frozen_string_literal: true

require_relative 'command'

# Command class in charge of execute the turn to the right of the robot
class Right < Command

  def initialize(robot)
    super()
    @robot = robot
  end

  def execute
    @robot.turn_right
  end
end
