# frozen_string_literal: true

require_relative 'command'

# Command class in charge of execute the report about where is the robot located
class Report < Command

  def initialize(robot)
    super()
    @robot = robot
  end

  def execute
    report = @robot.report

    report ? puts(report) : puts('Not ready yet')
  end
end
