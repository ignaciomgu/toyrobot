# frozen_string_literal: true

require_relative 'command'
require_relative '../../exceptions/out_of_range'
require_relative '../../common/compass_helper'

# Command class in charge of execute the placement of the robot within the board
class Place < Command
  def initialize(board, robot)
    super()
    @board = board
    @robot = robot
  end

  def execute
    axis_x = arguments[:x]
    axis_y = arguments[:y]
    direction = arguments[:direction]

    raise InvalidArguments unless CompassHelper::DIRECTIONS.include?(direction) && axis_x && axis_y
    raise OutOfRange unless @board.inside?(axis_x, axis_y)

    @robot.place(axis_x, axis_y, direction)
  end
end
