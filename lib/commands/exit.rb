# frozen_string_literal: true

require_relative 'command'

# Command class in charge of execute the exit of the program
class Exit < Command
  def execute
    puts 'Bye!'

    exit
  end
end
