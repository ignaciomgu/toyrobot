# frozen_string_literal: true

# Class parent of all the present commands
class Command
  attr_accessor :arguments

  def initialize
    @arguments = {}
  end

  # Need to be overwritten
  def execute
    raise NotImplementedError
  end

  # Print itself
  def to_string
    "Command: \"#{self.class.name}\" #{arguments}"
  end
end
