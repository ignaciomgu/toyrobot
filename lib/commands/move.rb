# frozen_string_literal: true

require_relative 'command'
require_relative '../../exceptions/out_of_range'

# Command class in charge of execute the move ahead of the robot
class Move < Command
  def initialize(board, robot)
    super()
    @board = board
    @robot = robot
  end

  def execute
    next_x = @robot.next_x
    next_y = @robot.next_y

    raise OutOfRange unless next_x.nil? || next_y.nil? || @board.inside?(next_x, next_y)

    @robot.move
  end
end
