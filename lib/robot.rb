# frozen_string_literal: true

require_relative '../common/compass_helper'
require_relative '../exceptions/invalid_arguments'

# This class represent the robot of the game that it is able to be placed in different coordinates and directions
class Robot
  attr_reader :axis_x, :axis_y, :direction

  def initialize(axis_x = nil, axis_y = nil, direction = nil)
    @axis_x = axis_x
    @axis_y = axis_y
    @direction = direction
  end

  def place(axis_x, axis_y, direction)
    @axis_x = axis_x
    @axis_y = axis_y
    @direction = direction
  end

  def move
    return unless ready?

    @axis_x = next_x
    @axis_y = next_y
  end

  def next_x
    @axis_x + CompassHelper::TO_X[@direction] if ready?
  end

  def next_y
    @axis_y + CompassHelper::TO_Y[@direction] if ready?
  end

  def turn_left
    @direction = CompassHelper::TO_LEFT[@direction] if ready?
  end

  def turn_right
    @direction = CompassHelper::TO_RIGHT[@direction] if ready?
  end

  def report
    "#{@axis_x},#{@axis_y},#{@direction}" if ready?
  end

  private

  def ready?
    !@axis_x.nil? && !@axis_y.nil? && !@direction.nil?
  end
end
